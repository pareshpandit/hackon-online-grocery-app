package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Selectproductbrands extends AppCompatActivity {
    Switch switch1,switch2,switch3,switch4,switch5;
    TextView prodname1,prodname2,prodname3,prodname4,prodname5;                                      //Using 5 because we are using 5 brands
    TextView proddescription1,proddescription2,proddescription3,proddescription4,proddescription5;
    ImageView prodimage1,prodimage2,prodimage3,prodimage4,prodimage5;

    public  String getstring;
    public  static  final String shared_pref="shared";
    public  static  final String TEXT="text";
    private  String recievedprodname;

    int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String mTitle[] = new String[]{"Milk", "Paneer", "Buttermilk", "Curd","Ghee","Cheese"};                  //Here you can fetch your data and it gets displayed on the app
        String mDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables","Add household care products","Add personal & baby care items"};  //Here you can fetch your data and it gets displayed on the app
        int images[] =  new int[]{R.drawable.milk, R.drawable.paneer, R.drawable.butter, R.drawable.curd,R.drawable.ghee,R.drawable.cheese};  //Here you can fetch your data and it gets displayed on the app

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectproductbrands);

        Intent intent = getIntent();

        recievedprodname=intent.getExtras().get("prodname").toString();

        Toast.makeText(Selectproductbrands.this, recievedprodname, Toast.LENGTH_SHORT).show();

        // Elements of UI are identified in this section
        prodimage1=findViewById(R.id.prodimage1);
        prodimage2=findViewById(R.id.prodimage2);
        prodimage3=findViewById(R.id.prodimage3);
        prodimage4=findViewById(R.id.prodimage4);
        prodimage5=findViewById(R.id.prodimage5);
        prodname1=findViewById(R.id.prodname1);
        prodname2=findViewById(R.id.prodname2);
        prodname3=findViewById(R.id.prodname3);
        prodname4=findViewById(R.id.prodname4);
        prodname5=findViewById(R.id.prodname5);
        proddescription1=findViewById(R.id.proddescription1);
        proddescription2=findViewById(R.id.proddescription2);
        proddescription3=findViewById(R.id.proddescription3);
        proddescription4=findViewById(R.id.proddescription4);
        proddescription5=findViewById(R.id.proddescription5);
        switch1=findViewById(R.id.switch1);
        switch2=findViewById(R.id.switch2);
        switch3=findViewById(R.id.switch3);
        switch4=findViewById(R.id.switch4);
        switch5=findViewById(R.id.switch5);
        ////


       // The resource values for UI elements are set in this section

        prodname1.setText(mTitle[0]);
        prodname2.setText(mTitle[1]);
        prodname3.setText(mTitle[2]);
        prodname4.setText(mTitle[3]);
        prodname5.setText(mTitle[4]);
        proddescription1.setText(mDescription[0]);
        proddescription2.setText(mDescription[1]);
        proddescription3.setText(mDescription[2]);
        proddescription4.setText(mDescription[3]);
        proddescription5.setText(mDescription[4]);
        prodimage1.setImageResource(images[0]);
        prodimage2.setImageResource(images[1]);
        prodimage3.setImageResource(images[2]);
        prodimage4.setImageResource(images[3]);
        prodimage5.setImageResource(images[4]);
//



        SharedPreferences sharedPreferences1=getSharedPreferences("save",MODE_PRIVATE);                 //Shared preference are used to store the state of switch so that it
        SharedPreferences sharedPreferences2=getSharedPreferences("save",MODE_PRIVATE);                 //doesn't change when the app is closed
        SharedPreferences sharedPreferences3=getSharedPreferences("save",MODE_PRIVATE);
        SharedPreferences sharedPreferences4=getSharedPreferences("save",MODE_PRIVATE);
        SharedPreferences sharedPreferences5=getSharedPreferences("save",MODE_PRIVATE);

            switch1.setChecked(sharedPreferences1.getBoolean("value1",true));
//        switch2.setChecked(sharedPreferences1.getBoolean("value2",true));

            switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {                                                //On click listener for switch button 1
                    if (isChecked) {
                        // The toggle is enabled
                        SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                        editor.putBoolean("value1",true);
                        editor.apply();
                        switch1.setChecked(true);
                    } else {
                        // The toggle is disabled
                        SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                        editor.putBoolean("value1",false);
                        editor.apply();
                        switch1.setChecked(false);
                    }
                }
            });
        switch2.setChecked(sharedPreferences2.getBoolean("value2",true));

        switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value2",true);
                    editor.apply();
                    switch2.setChecked(true);
                } else {
                    // The toggle is disabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value2",false);
                    editor.apply();
                    switch2.setChecked(false);
                }
            }
        });
        switch3.setChecked(sharedPreferences3.getBoolean("value3",true));

        switch3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value3",true);
                    editor.apply();
                    switch3.setChecked(true);
                } else {
                    // The toggle is disabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value3",false);
                    editor.apply();
                    switch3.setChecked(false);
                }
            }
        });
        switch4.setChecked(sharedPreferences4.getBoolean("value4",true));

        switch4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value4",true);
                    editor.apply();
                    switch4.setChecked(true);
                } else {
                    // The toggle is disabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value4",false);
                    editor.apply();
                    switch4.setChecked(false);
                }
            }
        });
        switch5.setChecked(sharedPreferences5.getBoolean("value5",true));

        switch5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value5",true);
                    editor.apply();
                    switch5.setChecked(true);
                } else {
                    // The toggle is disabled
                    SharedPreferences.Editor editor=getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value5",false);
                    editor.apply();
                    switch5.setChecked(false);
                }
            }
        });

    }
}
