package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class Selcategory extends AppCompatActivity {

    GridView gridView;
    String[] categoryNames = {"Grocery", "Dairy", "Medicine", "Vegetables", "Household care", "Personal & Baby Care", "Packaged Foods", "Snacks"}; //The categories names that are displayed in the gridView
    int[] categoryImages = {R.drawable.superm, R.drawable.dairyp, R.drawable.newmed, R.drawable.veg, R.drawable.housecare, R.drawable.bodycare, R.drawable.packfood, R.drawable.snack};  //Images for each category that are displayed in the Gridview

    String dTitle[] = new String[]{"Milk", "Paneer", "Buttermilk", "Curd","Ghee","Cheese"}; //If the person selects dairy category then we passs this array to the next activity which sets the name for products in that category
    String dDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables","Add household care products","Add personal & baby care items"}; //If the person selects dairy category then we passs this array to the next activity which sets the product description for that category
    int dimages[] =  new int[]{R.drawable.milk, R.drawable.paneer, R.drawable.butter, R.drawable.curd,R.drawable.ghee,R.drawable.cheese}; //If the person selects dairy category then we pass this array to the next activity which sets the images for products in that category


    //Similarly the bottom are used to pass the data to next activity if he chooses some other category
    String mTitle[] = new String[]{"Antiseptics", "Vicks", "Balm"};
    String mDescription[] =new String[]{"Add Grocery ", "Add Dairy products","Add Medicine"};


    int mimages[] =  new int[]{R.drawable.newmed, R.drawable.vicks, R.drawable.balm};

    String hTitle[] = new String[]{"Clothe Washing Soaps", "Dish Washing Soaps", "Detergent","Dishwash Liquids","Floor Cleaners","Toilet Cleaers","Broom","Clothe washing brush","Scrub & steel scrub"};

    String hDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables","Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables","Add Grocery"};


    int himages[] =  new int[]{R.drawable.clothesoap, R.drawable.dishsoap, R.drawable.detergent, R.drawable.dishwashgel,R.drawable.floorcleaners, R.drawable.toiletcleaner, R.drawable.brooms, R.drawable.clothbrush,R.drawable.scrub};

    String pTitle[] = new String[]{"Soaps", "Facewash", "Shampoo","Hairoil","Skin Powder","Deodrants & Perfume","Razors","Shaving Cream"};

    String pDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables","Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables"};


    int pimages[] =  new int[]{R.drawable.bathsoap, R.drawable.facewash, R.drawable.shampoo, R.drawable.hairoils,R.drawable.skinpowder, R.drawable.deodrants, R.drawable.razor, R.drawable.shavingcream};

    String sTitle[] = new String[]{"Buiscuits", "Chips", "kurkure","Fruit Cake"};

    String sDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products", "Add vegetables"};


    int simages[] =  new int[]{R.drawable.buiscuit, R.drawable.chips, R.drawable.kurkure, R.drawable.fruitcake};

    String packTitle[] = new String[]{"Maggi", "Soup", "Paazta"};

    String packDescription[] =new String[]{"Add Grocery ", "Add Dairy products", "Add Medicinal Products"};


    int packimages[] =  new int[]{R.drawable.maggi, R.drawable.soup, R.drawable.paazta};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selcategory);


        gridView = findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getApplicationContext(), Addtostore.class);  //shift fron this activity to the next activity that represents the products in that category
                if(categoryNames[i]=="Dairy")
                {

                    intent.putExtra("titles", dTitle);             //here we pass the product name array to the next activity that will display all the products
                    intent.putExtra("Description", dDescription);   //here we pass the product description  array to the next activity that will display all the products
                    intent.putExtra("images",dimages);              ////here we pass the product image array to the next activity that will display all the products


                }
                if(categoryNames[i]=="Medicine")
                {

                    intent.putExtra("titles", mTitle);
                    intent.putExtra("Description", mDescription);
                    intent.putExtra("images",mimages);


                }

                if(categoryNames[i]=="Household care")
                {

                    intent.putExtra("titles", hTitle);
                    intent.putExtra("Description", hDescription);
                    intent.putExtra("images",himages);


                }

                if(categoryNames[i]=="Personal & Baby Care")
                {

                    intent.putExtra("titles", pTitle);
                    intent.putExtra("Description", pDescription);
                    intent.putExtra("images",pimages);


                }

                if(categoryNames[i]=="Snacks")
                {

                    intent.putExtra("titles", sTitle);
                    intent.putExtra("Description", sDescription);
                    intent.putExtra("images",simages);


                }
                if(categoryNames[i]=="Packaged Foods")
                {

                    intent.putExtra("titles", packTitle);
                    intent.putExtra("Description", packDescription);
                    intent.putExtra("images",packimages);

                }
                if(categoryNames[i]=="Grocery")
                {

                    intent.putExtra("titles", sTitle);
                    intent.putExtra("Description", sDescription);
                    intent.putExtra("images",simages);


                }
                if(categoryNames[i]=="Vegetables")
                {

                    intent.putExtra("titles", packTitle);
                    intent.putExtra("Description", packDescription);
                    intent.putExtra("images",packimages);

                }





                startActivity(intent);

            }
        });
    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return categoryImages.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data, null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);          //here we set the resource value for each item in the gridview
            ImageView image = view1.findViewById(R.id.images);        //here we set the resource value for each item in the gridview

            name.setText(categoryNames[i]);
            image.setImageResource(categoryImages[i]);
            return view1;


        }
    }
}

