package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class ProfileInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info);


        Spinner shopcat = (Spinner) findViewById(R.id.shopcateg);
        Spinner homedel = (Spinner) findViewById(R.id.homedeldd);
        Spinner reqadvpay = (Spinner) findViewById(R.id.reqadvpaydd);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ProfileInfo.this,android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.shopcategnames));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shopcat.setAdapter(myAdapter);

        ArrayAdapter<String> myAdapter2 = new ArrayAdapter<String>(ProfileInfo.this,android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.homedelilist));
        myAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        homedel.setAdapter(myAdapter2);

        ArrayAdapter<String> myAdapter3 = new ArrayAdapter<String>(ProfileInfo.this,android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.reqadvpaylist));
        myAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reqadvpay.setAdapter(myAdapter3);



     }
}
