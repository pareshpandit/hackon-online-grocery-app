package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText mobile = (EditText) findViewById(R.id.mobile);
        final EditText password = (EditText) findViewById(R.id.password);
        Button register = findViewById(R.id.register);
        Button login = findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mobile.getText().length() == 0 || password.getText().length() == 0) {
                    Toast.makeText(Login.this, "Please fill all details ", Toast.LENGTH_SHORT).show();
                } else if (mobile.getText().length() < 10 || mobile.getText().length() > 10 && password.getText().length() >= 0) {

                    Toast.makeText(Login.this, "Phone No should be 10 digits long ", Toast.LENGTH_SHORT).show();
                } else if (mobile.getText().length() > 10 && password.getText().length() >= 0) {

                    Toast.makeText(Login.this, "Phone No should be 10 digits long ", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i1 = new Intent(Login.this, Home.class);
                    startActivity(i1);
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(Login.this, Register.class);
                startActivity(i2);
            }
        });


    }
}
