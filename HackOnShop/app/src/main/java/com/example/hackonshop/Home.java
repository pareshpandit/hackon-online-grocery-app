package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ImageView inventory= (ImageView) findViewById(R.id.inventory);
        inventory.bringToFront();
        inventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent1 = new Intent(Home.this, Selcategory.class);
                startActivity(myIntent1);
            }
        });

        ImageView active_orders= (ImageView) findViewById(R.id.active_orders);
        active_orders.bringToFront();
        active_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent2 = new Intent(Home.this, Activeorderlist.class);
                startActivity(myIntent2);
            }
        });

        ImageView history= (ImageView) findViewById(R.id.history);
        history.bringToFront();
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent3 = new Intent(Home.this, Selcategory.class);
                startActivity(myIntent3);
            }
        });

        ImageView feedback= (ImageView) findViewById(R.id.feedback);
        feedback.bringToFront();
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent4 = new Intent(Home.this, Selcategory.class);
                startActivity(myIntent4);
            }
        });

        ImageView profile= (ImageView) findViewById(R.id.profile);
        profile.bringToFront();
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent5 = new Intent(Home.this, Selcategory.class);
                startActivity(myIntent5);
            }
        });

        ImageView settings= (ImageView) findViewById(R.id.settings);
        settings.bringToFront();
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent6 = new Intent(Home.this, Settings.class);
                startActivity(myIntent6);
            }
        });

        ImageView demo= (ImageView) findViewById(R.id.demo);
        demo.bringToFront();
        demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myIntent7 = new Intent(Home.this, Selcategory.class);
//                startActivity(myIntent7);
                Intent open_demo = new Intent();
                open_demo.setAction(Intent.ACTION_VIEW);
                open_demo.addCategory(Intent.CATEGORY_BROWSABLE);
                open_demo.setData(Uri.parse("https://www.youtube.com/watch?v=h7gyJRWrjbg"));
                startActivity(open_demo);
            }
        });

    }
}
