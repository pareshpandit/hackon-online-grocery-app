package com.example.hackonshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Otp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Button verotp = findViewById(R.id.verifyotp);

        verotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(Otp.this, ProfileInfo.class);
                startActivity(i1);
                Toast.makeText(Otp.this, "Otp Verified ", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
