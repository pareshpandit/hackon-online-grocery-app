package com.example.hackonshop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Orderdetails extends AppCompatActivity {
ListView listView;
String recievedordername;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        Intent intent = getIntent();
        recievedordername=intent.getExtras().get("ordername").toString();
        listView = findViewById(R.id.listView);
        String mTitle[] = new String[]{"Product1", "Product2", "Product3", "Product4","Product5","Product6"};  //here we can fetech all the products that were mentioned in a particular order
        String mDescription[] =new String[]{"Quantity ", "Quantity", "Quantity", "Quantity","Quantity","Quantity"};
        String quantValues[] = new String[]{"1 Litre ", "2 Kg", "3 Litres", "250 gms","0.5 kg"," 2 kg"};
    int images[] =  new int[]{R.drawable.milk, R.drawable.paneer, R.drawable.butter, R.drawable.curd,R.drawable.ghee,R.drawable.cheese};
        Toast.makeText(Orderdetails.this, recievedordername, Toast.LENGTH_SHORT).show();
        // now create an adapter class

        Orderdetails.MyAdapter adapter = new Orderdetails.MyAdapter(this, mTitle, mDescription, quantValues,images);
        listView.setAdapter(adapter);

        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String currentordername= adapterView.getItemAtPosition(position).toString();
                Intent intent =new Intent(Orderdetails.this,Selectproductbrands.class);

//                if (position ==  0) {
//                    Toast.makeText(Addtostore.this, "Facebook Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  1) {
//                    Toast.makeText(Addtostore.this, "Whatsapp Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  2) {
//                    Toast.makeText(Addtostore.this, "Twitter Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  3) {
//                    Toast.makeText(Addtostore.this, "Instagram Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  4) {
//                    Toast.makeText(Addtostore.this, "Youtube Description", Toast.LENGTH_SHORT).show();
//                }

            }
        });
        // so item click is done now check list view
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        String rValues[];
        int rImgs[];

        MyAdapter (Context c, String title[], String description[], String quantityValues[],int imgs[]) {
            super(c, R.layout.orderdetails_row, R.id.prodname, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;
            this.rValues=quantityValues;
            this.rImgs = imgs;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.orderdetails_row, parent, false);
            ImageView images = row.findViewById(R.id.prodimage);
            TextView myTitle = row.findViewById(R.id.prodname);
            TextView myDescription = row.findViewById(R.id.proddescription);

            TextView myQuantity=  row.findViewById(R.id.quantvalue);

            // now set our resources on views
            images.setImageResource(rImgs[position]);      //here we set the resource values for the UI elements
            myTitle.setText(rTitle[position]);              //here we set the resource values for the UI elements
            myQuantity.setText(rValues[position]);           //here we set the resource values for the UI elements
            myDescription.setText(rDescription[position]);    //here we set the resource values for the UI elements
            return row;
        }
    }
}
