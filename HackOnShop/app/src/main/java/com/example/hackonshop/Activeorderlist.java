package com.example.hackonshop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Activeorderlist extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activeorderlist);
        listView = findViewById(R.id.listView);

        String mTitle[] = new String[]{"Order1", "Order2", "Order3", "Order4","Order5","Order6"};                       //We can fetch the orders according to the Shop Name/ID here
    String mDescription[] =new String[]{"Order details ", "Order details", "Order details", "Order details","Order details","Order details"};

        Activeorderlist.MyAdapter adapter = new Activeorderlist.MyAdapter(this, mTitle, mDescription);
        listView.setAdapter(adapter);

        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String currentordername= adapterView.getItemAtPosition(position).toString();
                Intent intent =new Intent(Activeorderlist.this,Orderdetails.class);

                intent.putExtra("ordername",currentordername);
                startActivity(intent);
//                if (position ==  0) {
//                    Toast.makeText(Addtostore.this, "Facebook Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  1) {
//                    Toast.makeText(Addtostore.this, "Whatsapp Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  2) {
//                    Toast.makeText(Addtostore.this, "Twitter Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  3) {
//                    Toast.makeText(Addtostore.this, "Instagram Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  4) {
//                    Toast.makeText(Addtostore.this, "Youtube Description", Toast.LENGTH_SHORT).show();
//                }

            }
        });
        // so item click is done now check list view
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        MyAdapter(Context c, String[] title, String[] description) {
            super(c, R.layout.row, R.id.prodname, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.activeorder_row, parent, false);

            TextView myTitle = row.findViewById(R.id.ordername);
            TextView myDescription = row.findViewById(R.id.orderdescription);

            // now set our resources on views
            myTitle.setText(rTitle[position]);                  //here we set the resource values for the UI element
            myDescription.setText(rDescription[position]);      //here we set the resource values for the UI element
            return row;
        }
    }
}

