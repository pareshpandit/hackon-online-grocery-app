package com.example.hackonshop;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Addtostore extends AppCompatActivity {
    ListView listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addtostore);
        Intent intent = getIntent();
        final String mTitle[] = intent.getStringArrayExtra("titles");              //here we recieve the array that was passed from the previous activity
        final String mDescription[] = intent.getStringArrayExtra("Description");   //here we recieve the array that was passed from the previous activity
        final int[] images= intent.getIntArrayExtra("images");                     //here we recieve the array that was passed from the previous activity

        listView = findViewById(R.id.listView);

        MyAdapter adapter = new MyAdapter(this, mTitle, mDescription, images);
        listView.setAdapter(adapter);


        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String currentprodname= adapterView.getItemAtPosition(position).toString();
                Intent intent =new Intent(Addtostore.this,Selectproductbrands.class);

                intent.putExtra("prodname",currentprodname);
                startActivity(intent);
//                if (position ==  0) {
//                    Toast.makeText(Addtostore.this, "Facebook Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  1) {
//                    Toast.makeText(Addtostore.this, "Whatsapp Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  2) {
//                    Toast.makeText(Addtostore.this, "Twitter Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  3) {
//                    Toast.makeText(Addtostore.this, "Instagram Description", Toast.LENGTH_SHORT).show();
//                }
//                if (position ==  4) {
//                    Toast.makeText(Addtostore.this, "Youtube Description", Toast.LENGTH_SHORT).show();
//                }

//                if(mTitle[position]=="Milk")
//                {
//                    Toast.makeText(Addtostore.this, mTitle[position], Toast.LENGTH_SHORT).show();
//

//                }

            }
        });
        // so item click is done now check list view
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        MyAdapter (Context c, String title[], String description[], int imgs[]) {
            super(c, R.layout.row, R.id.prodname, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;
            this.rImgs = imgs;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.prodimage);                     //set the resource value for each item in the listView
            TextView myTitle = row.findViewById(R.id.prodname);                       //set the resource value for each item in the listView
            TextView myDescription = row.findViewById(R.id.proddescription);           //set the resource value for each item in the listView

            // now set our resources on views
            images.setImageResource(rImgs[position]);
            myTitle.setText(rTitle[position]);
            myDescription.setText(rDescription[position]);
            return row;
        }
    }
}

